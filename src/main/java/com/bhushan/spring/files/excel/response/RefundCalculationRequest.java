package com.bhushan.spring.files.excel.response;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.model.ReturnProductName;
import com.bhushan.spring.files.excel.model.ReturnProductnames;

@Component
public class RefundCalculationRequest {
	
	private int id;
	private List<ReturnProductName> returnedproduct;
	private List<Productnames>  productpurchased;
	private double totalamount;
	private double deliverycharges;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public List<ReturnProductName> getReturnedproduct() {
		return returnedproduct;
	}
	public void setReturnedproduct(List<ReturnProductName> returnedproduct) {
		this.returnedproduct = returnedproduct;
	}
	public List<Productnames> getProductpurchased() {
		return productpurchased;
	}
	public void setProductpurchased(List<Productnames> productpurchased) {
		this.productpurchased = productpurchased;
	}
	public double getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}
	public double getDeliverycharges() {
		return deliverycharges;
	}
	public void setDeliverycharges(double deliverycharges) {
		this.deliverycharges = deliverycharges;
	}

}