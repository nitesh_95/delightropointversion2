package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.PaymentHistory;
import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.model.PromocodeOffers;
import com.bhushan.spring.files.excel.model.PurchaseDetails;
import com.bhushan.spring.files.excel.model.PurchasedProduct;
import com.bhushan.spring.files.excel.model.ReturnOnlineCustomer;
import com.bhushan.spring.files.excel.model.ReturnPaymentHistory;
import com.bhushan.spring.files.excel.model.ReturnProductName;
import com.bhushan.spring.files.excel.repository.OnlineCustomerRequestRepo;
import com.bhushan.spring.files.excel.repository.PaymentHistoryRepo;
import com.bhushan.spring.files.excel.repository.ProductPurchasedRepo;
import com.bhushan.spring.files.excel.repository.ProductRepository;
import com.bhushan.spring.files.excel.repository.PromocodeOfferRepo;
import com.bhushan.spring.files.excel.repository.PurchaseDetailsRepo;
import com.bhushan.spring.files.excel.repository.ReturnOnlineProduct;
import com.bhushan.spring.files.excel.response.OnlineCustomerResponse;
import com.bhushan.spring.files.excel.response.PromocodeOfferResponse;
import com.bhushan.spring.files.excel.response.RefundCalculationRequest;
import com.bhushan.spring.files.excel.response.ReturnOnlineCustomerResponse;

@Service
public class OnlineCustomerService {

	@Autowired
	ProductRepository repository;

	@Autowired
	OnlineCustomerRequestRepo customerProductSaveRepo;
	@Autowired
	PaymentHistoryRepo paymentHistoryRepo;
	@Autowired
	ProductPurchasedRepo productpurchasedrepo;
	@Autowired
	PurchaseDetailsRepo purchasedetailsrepo;
	@Autowired
	private MessageUtil messageUtill;
	@Autowired
	private ReturnOnlineProduct onlineProduct;
	@Autowired
	private OnlineCustomerRequestRepo customerRequestRepo;
	@Autowired
	private PromocodeOfferRepo promocodeOfferRepo;

	public Double findbyPromocode(String promocode) {
		Optional<PromocodeOffers> findbyPromoCode = promocodeOfferRepo.findbyPromoCode(promocode);
		if (findbyPromoCode.isPresent() && findbyPromoCode.get().isPromocodeused() == false) {
			System.out.println(findbyPromoCode.get().getPromocodevalue());
			return findbyPromoCode.get().getPromocodevalue();
		} else {
			return 0.0;
		}
	}

	public PromocodeOfferResponse disbursePromocode(String promocode) throws Exception {
		PromocodeOfferResponse promocodeOfferResponse = new PromocodeOfferResponse();
		PromocodeOffers promocodeOffers = new PromocodeOffers();
		Optional<PromocodeOffers> findbyPromoCode = promocodeOfferRepo.findbyPromoCode(promocode);
		if (findbyPromoCode.isPresent() && findbyPromoCode.get().isPromocodeused() == false) {
			promocodeOffers.setId(findbyPromoCode.get().getId());
			promocodeOffers.setPromocode(promocode);
			promocodeOffers.setPromocodegenerationdate(findbyPromoCode.get().getPromocodegenerationdate());
			promocodeOffers.setPromocodeused(true);
			promocodeOffers.setPromocodevalue(findbyPromoCode.get().getPromocodevalue());
			PromocodeOffers save = promocodeOfferRepo.save(promocodeOffers);
			if (save != null) {
				messageUtill.disburseAmount(save);
				messageUtill.disburseAmountInfoToAdmin(save);
				promocodeOfferResponse.setMessage("Data has been retrieved");
				promocodeOfferResponse.setStatus("200");
				promocodeOfferResponse.setPromocodeOffers(save);
			}
		} else {
			promocodeOfferResponse.setMessage("something went wrong");
			promocodeOfferResponse.setStatus("500");
			promocodeOfferResponse.setPromocodeOffers(null);
		}
		return promocodeOfferResponse;
	}

	public Optional<OnlineCustomerRequest> saveData(OnlineCustomerRequest response)
			throws ClientProtocolException, IOException {
		PaymentHistory paymentHistory = new PaymentHistory();
		String generatedString = RandomStringUtils.randomNumeric(6);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		response.getPurchasedetails().setPurchasedate(timeStamp);
		int parseInt = Integer.parseInt(generatedString);
		response.setInvoiceno(parseInt);
		System.out.println(response);
		for (int i = 0; i < response.getProductpurchased().size(); i++) {
			response.getProductpurchased().get(i).setDateofpurchase(timeStamp);
			response.getProductpurchased().get(i).setTypeofcustomer("ONLINE_CUSTOMER");
		}

		int setPromocodeuser = promocodeOfferRepo.setPromocodeuser(response.getPaymenthistory().getPromocodeapplieds());
		if (setPromocodeuser > 0) {
			response.getPurchasedetails().setAmountafterpromocode(
					response.getPurchasedetails().getTotalamount() - response.getPaymenthistory().getPromocodevalues());
		} else {
			response.getPurchasedetails().setAmountafterpromocode(response.getPurchasedetails().getTotalamount());
		}

		OnlineCustomerRequest save = customerProductSaveRepo.save(response);
		messageUtill.sendOnlineCustomerDetails(save);
		messageUtill.sendOnlineCustomerDetailstoadmin(save);
		return customerProductSaveRepo.findById(save.getId());
	}

	public List<OnlineCustomerRequest> getListOfData() {
		List<OnlineCustomerRequest> findAll = customerProductSaveRepo.findAll();
		return findAll;
	}

	public OnlineCustomerRequest updateData(OnlineCustomerRequest response) throws Exception {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		response.getPurchasedetails().setPurchasedate(timeStamp);
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(response.getId());
		if (findById.isPresent()) {
			PurchasedProduct productPurchased = new PurchasedProduct();
			PaymentHistory paymentHistory = new PaymentHistory();
			OnlineCustomerRequest productPurchasedSaveDetails = findById.get();
			productPurchasedSaveDetails.setAddress(response.getAddress());
			productPurchasedSaveDetails.setMailid(response.getMailid());
			productPurchasedSaveDetails.setMobileno(response.getMobileno());
			productPurchasedSaveDetails.setName(response.getName());
			if (response.isIs_cancelled() == true) {
				productPurchasedSaveDetails.setIs_cancelled(response.isIs_cancelled());
				productPurchasedSaveDetails.setCancelleddate(timeStamp);
				messageUtill.orderCancellation(productPurchasedSaveDetails);
				messageUtill.orderCancellationAdmin(productPurchasedSaveDetails);
			}

			paymentHistory.setDateofdelivery(response.getPaymenthistory().getDateofdelivery());
			paymentHistory.setOrderstatus(response.getPaymenthistory().getOrderstatus());
			paymentHistory.setPaymentdate(response.getPaymenthistory().getPaymentdate());
			paymentHistory.setPaymentstatus(response.getPaymenthistory().getPaymentstatus());
			paymentHistory.setTransactionid(response.getPaymenthistory().getTransactionid());
			paymentHistory.setDeliverycharges(response.getPaymenthistory().getDeliverycharges());
			paymentHistory.setPromocodeapplieds(findById.get().getPaymenthistory().getPromocodeapplieds());
			paymentHistory.setPromocodevalues(findById.get().getPaymenthistory().getPromocodevalues());
			if (paymentHistory.getDeliverycharges() == 0) {
				paymentHistory.setTotalcostAfterapplyingdeliverycharges(
						response.getPurchasedetails().getTotalamount() + paymentHistory.getDeliverycharges()
								- response.getPurchasedetails().getAmountafterpromocode());
			} else {
				paymentHistory.setTotalcostAfterapplyingdeliverycharges(
						response.getPurchasedetails().getTotalamount() + paymentHistory.getDeliverycharges()
								- response.getPurchasedetails().getAmountafterpromocode());
			}
//			productPurchased.setWarrantydays(response.getProductpurchased().get(0).getWarrantydays());
			productPurchasedSaveDetails.setPaymenthistory(paymentHistory);
			productPurchasedSaveDetails.setPostalcode(response.getPostalcode());
			productPurchasedSaveDetails.setProductpurchased(response.getProductpurchased());
			productPurchasedSaveDetails.setPurchasedetails(response.getPurchasedetails());
			productPurchasedSaveDetails = customerProductSaveRepo.save(productPurchasedSaveDetails);
			if (productPurchasedSaveDetails.isIs_cancelled() != true) {
				messageUtill.onlineOrderValueUpdateByAdmin(productPurchasedSaveDetails);
			}
			return productPurchasedSaveDetails;
		} else {
			throw new Exception("The Id is not Present");
		}
	}

	public Optional<OnlineCustomerRequest> updateDatas(RefundCalculationRequest response) throws Exception {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(response.getId());
		if (findById.isPresent()) {
			OnlineCustomerRequest onlineCustomerRequest = new OnlineCustomerRequest();
			onlineCustomerRequest.setId(response.getId());
			onlineCustomerRequest.setName(findById.get().getName());
			onlineCustomerRequest.setMailid(findById.get().getMailid());
			onlineCustomerRequest.setAddress(findById.get().getAddress());
			onlineCustomerRequest.setPostalcode(findById.get().getPostalcode());
			onlineCustomerRequest.setMobileno(findById.get().getMobileno());
			onlineCustomerRequest.setInvoiceno(findById.get().getInvoiceno());

			for (int i = 0; i < response.getReturnedproduct().size(); i++) {
				response.getReturnedproduct().get(i)
						.setDateofpurchase(response.getReturnedproduct().get(i).getDateofpurchase());
				response.getReturnedproduct().get(i).setDateofreturn(timeStamp);
				response.getReturnedproduct().get(i)
						.setDescription(response.getReturnedproduct().get(i).getDescription());
				response.getReturnedproduct().get(i).setImagepath(response.getReturnedproduct().get(i).getImagepath());
				response.getReturnedproduct().get(i).setIsreturned(true);
				response.getReturnedproduct().get(i).setProductid(response.getReturnedproduct().get(i).getProductid());
				response.getReturnedproduct().get(i)
						.setProductname(response.getReturnedproduct().get(i).getProductname());
				response.getReturnedproduct().get(i)
						.setPurchaseamount(response.getReturnedproduct().get(i).getPurchaseamount());
				response.getReturnedproduct().get(i).setQuantity(response.getReturnedproduct().get(i).getQuantity());
				response.getReturnedproduct().get(i)
						.setTotalamount(response.getReturnedproduct().get(i).getTotalamount());
				response.getReturnedproduct().get(i)
						.setTypeofcustomer(response.getReturnedproduct().get(i).getTypeofcustomer());
				response.getReturnedproduct().get(i).setUnitprice(response.getReturnedproduct().get(i).getUnitprice());
				response.getReturnedproduct().get(i)
						.setWarrantydays(response.getReturnedproduct().get(i).getWarrantydays());
			}
			onlineCustomerRequest.setProductpurchased(findById.get().getProductpurchased());
			onlineCustomerRequest.setPaymenthistory(findById.get().getPaymenthistory());
			onlineCustomerRequest.setPurchasedetails(findById.get().getPurchasedetails());
			onlineCustomerRequest.setReturnproductname(response.getReturnedproduct());
			String promocode = RandomStringUtils.randomAlphanumeric(7).toUpperCase();
			PromocodeOffers offer = new PromocodeOffers();
			offer.setPromocode(promocode);
			offer.setPromocodeused(false);
			offer.setMobileno(onlineCustomerRequest.getMobileno());
			offer.setPromocodegenerationdate(timeStamp);
			offer.setPromocodevalue(refundmoney(onlineCustomerRequest));
			onlineCustomerRequest.setPromocodeoffer(offer);
			OnlineCustomerRequest save = customerProductSaveRepo.save(onlineCustomerRequest);
			if (save.getReturnproductname() != null && !save.getReturnproductname().isEmpty()) {
				messageUtill.returnProduct(save);
			}
			return customerProductSaveRepo.findById(save.getId());
		} else {
			throw new Exception("The Id is not Present");
		}
	}

	public ReturnOnlineCustomerResponse updateReturnRequest(int invoiceno1, long mobileno, int invoice2)
			throws Exception {
		ReturnOnlineCustomerResponse returnOnlineCustomerResponse = new ReturnOnlineCustomerResponse();
		ReturnOnlineCustomer returnOnlineCustomer = new ReturnOnlineCustomer();
		Optional<OnlineCustomerRequest> findbyinvoiceandmobilenos = customerRequestRepo
				.findbyinvoiceandmobileno(invoice2, mobileno);
		Optional<OnlineCustomerRequest> findbyinvoiceandmobileno = customerRequestRepo
				.findbyinvoiceandmobileno(invoiceno1, mobileno);
		OnlineCustomerRequest response = findbyinvoiceandmobileno.get();
		if (findbyinvoiceandmobileno.isPresent()) {
			returnOnlineCustomer.setName(response.getName());
			returnOnlineCustomer.setMailid(response.getMailid());
			returnOnlineCustomer.setAddress(response.getAddress());
			returnOnlineCustomer.setPostalcode(response.getPostalcode());
			returnOnlineCustomer.setMobileno(response.getMobileno());
			returnOnlineCustomer.setInvoiceno(response.getInvoiceno());
			returnOnlineCustomer.setEarlierinvoiceno(invoiceno1);
			returnOnlineCustomer.setProductpurchased(returnOnlineCustomer.getProductpurchased());
			ReturnOnlineCustomer save = onlineProduct.save(returnOnlineCustomer);
			returnOnlineCustomerResponse.setStatus("200");
			returnOnlineCustomerResponse.setMessage("Data Retrieved successfully");
			returnOnlineCustomerResponse.setOnlineCustomer(save);
		} else {
			returnOnlineCustomerResponse.setStatus("400");
			returnOnlineCustomerResponse.setMessage("No such Data exists as per our record");
			returnOnlineCustomerResponse.setOnlineCustomer(null);
		}
		return returnOnlineCustomerResponse;
	}

	public OnlineCustomerResponse findonlinecustomerbyinvoiceandmobileno(int invoiceno, long mobileno) {
		OnlineCustomerResponse onlinecustomerresponse = new OnlineCustomerResponse();
		Optional<OnlineCustomerRequest> findbyinvoiceandmobile = customerProductSaveRepo
				.findbyinvoiceandmobileno(invoiceno, mobileno);
		if (findbyinvoiceandmobile.isPresent()) {
			onlinecustomerresponse.setMessage("Data Retrieved");
			onlinecustomerresponse.setStatus("200");
			onlinecustomerresponse.setProductpurchaseddetails(findbyinvoiceandmobile.get());
		} else {
			onlinecustomerresponse.setMessage("Data not Available");
			onlinecustomerresponse.setStatus("500");
		}
		return onlinecustomerresponse;
	}

	public OnlineCustomerResponse getOnlineCustomer(int invoiceno, long mobileno) {
		OnlineCustomerResponse onlinecustomerresponse = new OnlineCustomerResponse();
		Optional<OnlineCustomerRequest> findbyinvoiceandmobile = customerProductSaveRepo
				.findbyinvoiceandmobileno(invoiceno, mobileno);
		if (findbyinvoiceandmobile.isPresent()) {
			onlinecustomerresponse.setMessage("Data Retrieved");
			onlinecustomerresponse.setStatus("200");
			onlinecustomerresponse.setProductpurchaseddetails(findbyinvoiceandmobile.get());
		} else {
			onlinecustomerresponse.setMessage("Data not Available");
			onlinecustomerresponse.setStatus("500");
		}
		return onlinecustomerresponse;
	}

	public OnlineCustomerRequest findbyid(int id) throws Exception {
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(id);
		if (findById.isPresent()) {
			return findById.get();
		} else {
			throw new Exception("Something is wrong");
		}

	}

	public OnlineCustomerRequest findbyids(int id) throws Exception {
		System.out.println("Id is" + id);
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(id);
		System.out.println(findById.get());
		if (findById.isPresent()) {
			OnlineCustomerRequest test = findById.get();
			return test;
		} else {
			throw new Exception("Customer not Found Exception");
		}

	}

	public double refundmoney(OnlineCustomerRequest onlineCustomerRequest) {
		double sum = 0;
		for (int i = 0; i < onlineCustomerRequest.getReturnproductname().size(); i++) {
			double totalamount = onlineCustomerRequest.getReturnproductname().get(i).getTotalamount();
			sum = sum + totalamount;
		}

		return sum - onlineCustomerRequest.getPaymenthistory().getDeliverycharges();
	}

	public double refundmoney(RefundCalculationRequest onlineCustomerRequest) {
		double sum = 0;
		for (ReturnProductName productname : onlineCustomerRequest.getReturnedproduct()) {
			double totalamount = productname.getTotalamount();
			sum = sum + totalamount;
		}

		return sum - onlineCustomerRequest.getDeliverycharges();
	}

}
