package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.EmployeeEntity;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.ServicingEntity;
import com.bhushan.spring.files.excel.repository.EmployeeDetailsRepo;
import com.bhushan.spring.files.excel.repository.OnlineCustomerRequestRepo;
import com.bhushan.spring.files.excel.repository.ServicingdetailsRepo;
import com.bhushan.spring.files.excel.response.ServicingEntityResponsee;

@Service
public class Servicingdetailsservice {

	@Autowired
	private ServicingdetailsRepo servicingdetailsRepo;
	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private OnlineCustomerRequestRepo onlinecustomerrequestrepo;
	@Autowired
	private EmployeeDetailsRepo employeedetails;

	public ServicingEntity savedata(ServicingEntity servicingEntity) throws ClientProtocolException, IOException {
		Optional<ServicingEntity> findById = servicingdetailsRepo.findById(servicingEntity.getCustomerids());
		if (findById.isPresent()) {
			servicingEntity.setPaidservicingamount(servicingEntity.getPaidservicingamount());
			servicingEntity.setRemainingamount(
					servicingEntity.getRemainingamount() - servicingEntity.getPaidservicingamount());
			servicingEntity.setTicketstatus(servicingEntity.getTicketstatus());
			servicingEntity.setDateofresolvingissue(servicingEntity.getDateofresolvingissue());
			System.out.println(generateOTP());
			servicingEntity.setOtp(generateOTP());
			ServicingEntity servicingentity = servicingdetailsRepo.save(servicingEntity);
			String assignedto = servicingEntity.getAssignedto();
			Optional<EmployeeEntity> findbyname = employeedetails.findbyname(assignedto);
			long mobileno = findbyname.get().getMobileno();
			return servicingentity;
		} else {
			String generatedString = RandomStringUtils.randomNumeric(7);
			servicingEntity.setTicketno(generatedString);
			servicingEntity.setDateofpurchase(servicingEntity.getDateofpurchase());
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 7);
			String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(c.getTime());
			double techniciancost = servicingEntity.getTechniciancost();
			double charge = servicingEntity.getCharge();
			float tax = servicingEntity.getTax();
			servicingEntity.setDateofservicerequest(timeStamp);
			servicingEntity.setOtp(generateOTP());
			servicingEntity.setTotalcost((charge + techniciancost) + (charge + techniciancost) * tax / 100);
			servicingEntity.setRemainingamount(servicingEntity.getTotalcost() - servicingEntity.getPaidamount());
			servicingEntity.setDateofresolvingissue(timeStamp);
			ServicingEntity servicingentity = servicingdetailsRepo.save(servicingEntity);
			messageUtil.sendServicingMessage(servicingEntity);
			messageUtil.sendTechnicianMsg(servicingentity, servicingentity.getMobileno());
			return servicingentity;
		}
	}

	public ServicingEntity saveonlineservicingdetails(ServicingEntity servicingEntity, int invoiceno, Long mobileno)
			throws Exception {
		Optional<OnlineCustomerRequest> findById = onlinecustomerrequestrepo.findbyinvoiceandmobileno(invoiceno,
				mobileno);
		Optional<ServicingEntity> findById2 = servicingdetailsRepo.findById(servicingEntity.getCustomerids());
		if (findById.isPresent() && findById2.isPresent()) {
			String generatedString = RandomStringUtils.randomNumeric(7);
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 7);
			String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(c.getTime());
			servicingEntity.setTicketstatus("Confirmed");
			servicingEntity.setServicingdate(timeStamp);
			servicingEntity.setOtp(generateOTP());
			servicingEntity.setTicketno(generatedString);
			servicingEntity.setDateofservicerequest(timeStamp);
			ServicingEntity servicingEntities = servicingdetailsRepo.save(servicingEntity);
			Optional<EmployeeEntity> findbyname = employeedetails.findbyname(servicingEntities.getAssignedto());
			messageUtil.sendTechnicianMsg(servicingEntities, findbyname.get().getMobileno());
			messageUtil.sendServicingMessage(servicingEntities);
			return servicingEntities;
		} else if (findById.isPresent()) {
			String timeStamps = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
			servicingEntity.setCustomerids(findById.get().getId());
			servicingEntity.setName(findById.get().getName());
			servicingEntity.setMobileno(findById.get().getMobileno());
			servicingEntity.setMailid(findById.get().getMailid());
			servicingEntity.setAddress(findById.get().getAddress());
			servicingEntity.setCustomertype("Online Customer");
			String invoiceNo = String.valueOf(findById.get().getInvoiceno());
			servicingEntity.setInvoiceno(invoiceNo);
			servicingEntity.setProductname(servicingEntity.getProductname());
			servicingEntity.setIssuedescription(servicingEntity.getIssuedescription());
			servicingEntity.setDateofservicerequest(timeStamps);
			servicingEntity.setTicketstatus("PENDING");
			ServicingEntity servicingEntities = servicingdetailsRepo.save(servicingEntity);
			messageUtil.sendonlineservicingmessage(servicingEntities.getName(), servicingEntities.getMobileno());
			return servicingEntities;
		} else {
			throw new Exception("Something went wrong");
		}
	}

	public ServicingEntityResponsee getbyticketnoandmobileno(String ticketno, Long mobileno) {
		ServicingEntityResponsee servicingEntityResponsee = new ServicingEntityResponsee();
		Optional<ServicingEntity> findbyticketnoandmobileno = servicingdetailsRepo.findbyticketnoandmobileno(ticketno,
				mobileno);
		String ticketstatus = findbyticketnoandmobileno.get().getTicketstatus();
		if (ticketstatus.equalsIgnoreCase("Completed")) {
			servicingEntityResponsee.setStatus("405");
			servicingEntityResponsee.setMessage("This Task has been completed");
			servicingEntityResponsee.setServicingEntity(null);
		} else if (ticketstatus.equalsIgnoreCase("PENDING")) {
			servicingEntityResponsee.setStatus("200");
			servicingEntityResponsee.setMessage("Data Retrieved Successfully");
			servicingEntityResponsee.setServicingEntity(findbyticketnoandmobileno.get());
		} else {
			servicingEntityResponsee.setStatus("400");
			servicingEntityResponsee.setMessage("WrongDetails");
			servicingEntityResponsee.setServicingEntity(null);
		}
		return servicingEntityResponsee;
	}

	public double gettotalamount(ServicingEntity servicingEntity) {
		double techniciancost = servicingEntity.getTechniciancost();
		double charge = servicingEntity.getCharge();
		float tax = servicingEntity.getTax();
		double totalcost = (charge + techniciancost) + (charge + techniciancost) * tax / 100;
		return totalcost;
	}

	public ServicingEntity findbyid(int id) throws Exception {
		Optional<ServicingEntity> findById = servicingdetailsRepo.findById(id);
		if (findById.isPresent()) {
			return findById.get();
		} else {
			throw new Exception("Id not Present");
		}
	}

	public ServicingEntityResponsee resendOtp(String ticketno, Long mobileno)
			throws ClientProtocolException, IOException {
		ServicingEntityResponsee servicingEntityResponsee = new ServicingEntityResponsee();
		Optional<ServicingEntity> findbyticketnoandmobileno = servicingdetailsRepo.findbyticketnoandmobileno(ticketno,
				mobileno);
		if (findbyticketnoandmobileno.isPresent()) {
			servicingEntityResponsee.setStatus("200");
			servicingEntityResponsee.setMessage("Data Retrieved Successfully");
			messageUtil.resendOtp(findbyticketnoandmobileno.get());
			servicingEntityResponsee.setServicingEntity(findbyticketnoandmobileno.get());
		} else {
			servicingEntityResponsee.setStatus("400");
			servicingEntityResponsee.setMessage("WrongDetails");
			servicingEntityResponsee.setServicingEntity(null);
		}
		return servicingEntityResponsee;
	}

	public ServicingEntityResponsee otpverification(String otp, String ticketno, Long mobileno)
			throws ClientProtocolException, IOException {
		ServicingEntityResponsee servicingEntityResponsee = new ServicingEntityResponsee();
		Optional<ServicingEntity> findbyticketnoandmobileno = servicingdetailsRepo.findbyticketnoandmobileno(ticketno,
				mobileno);
		String otp2 = findbyticketnoandmobileno.get().getOtp();
		if (otp.equalsIgnoreCase(otp2)) {
			servicingEntityResponsee.setStatus("200");
			servicingEntityResponsee.setMessage("OTP verified Successfully");
		} else {
			servicingEntityResponsee.setStatus("400");
			servicingEntityResponsee.setMessage("Wrong Otp try again");
		}
		return servicingEntityResponsee;
	}

	public List<ServicingEntity> findall() {
		List<ServicingEntity> findAll = servicingdetailsRepo.findAll();
		return findAll;
	}

	public static String generateOTP() {
		int randomPin = (int) (Math.random() * 9000) + 1000;
		String otp = String.valueOf(randomPin);
		return otp;
	}
}