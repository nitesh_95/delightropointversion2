package com.bhushan.spring.files.excel.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.helper.ExcelHelper;
import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;
import com.bhushan.spring.files.excel.model.FeedbackForm;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.ServiceFeedbackForm;
import com.bhushan.spring.files.excel.repository.FeedbackRepo;
import com.bhushan.spring.files.excel.repository.OfflineCustomerDetailsRepo;
import com.bhushan.spring.files.excel.repository.OnlineCustomerRequestRepo;
import com.bhushan.spring.files.excel.repository.PaymentHistoryRepo;
import com.bhushan.spring.files.excel.repository.ProductDetailsDataRepo;
import com.bhushan.spring.files.excel.repository.ProductPurchasedRepo;
import com.bhushan.spring.files.excel.repository.ProductRepository;
import com.bhushan.spring.files.excel.repository.PurchaseDetailsRepo;
import com.bhushan.spring.files.excel.repository.ServiceFeedbackFormRepo;
import com.bhushan.spring.files.excel.response.CustomerWarrantyRequest;
import com.bhushan.spring.files.excel.response.CustomerWarrantyResponse;
import com.bhushan.spring.files.excel.response.OfflineCustomerResponse;

@Service
public class ExcelService {
	@Autowired
	ProductRepository repository;

	@Autowired
	OnlineCustomerRequestRepo customerProductSaveRepo;
	@Autowired
	PaymentHistoryRepo paymentHistoryRepo;
	@Autowired
	ProductPurchasedRepo productpurchasedrepo;
	@Autowired
	PurchaseDetailsRepo purchasedetailsrepo;
	@Autowired
	FeedbackRepo feedbackrepo;
	@Autowired
	ServiceFeedbackFormRepo feedbackreposrepo;
	@Autowired
	ProductDetailsDataRepo productDetailsDataRepo;
	@Autowired
	private OfflineCustomerDetailsRepo offlinecustomerdetailsrepo;

	public void save() throws FileNotFoundException {
		File file = new File("/home/moglix/Downloads/Test.xlsx");
		InputStream targetStream = new FileInputStream(file);
		List<ExcelSheetToDBProductDetails> tutorials = ExcelHelper.excelToTutorials(targetStream);
		repository.saveAll(tutorials);
	}

	public OfflineCustomerDetails findofflinecustomerbyid(int id) {
		Optional<OfflineCustomerDetails> findById = offlinecustomerdetailsrepo.findById(id);
		return findById.get();
	}

	public OnlineCustomerRequest findbycustomeridandmobileno(int customerid, long mobileno) throws Exception {
		OnlineCustomerRequest findbycustomeridandmobileno = customerProductSaveRepo
				.findbycustomeridandmobileno(customerid, mobileno);
		return findbycustomeridandmobileno;
	}

	public FeedbackForm saveData(FeedbackForm feedbackForm) throws Exception {
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(feedbackForm.getCustomerid());
		if (findById.isPresent()) {
			FeedbackForm save = feedbackrepo.save(feedbackForm);
			return save;
		} else {
			throw new Exception("Record not Found");
		}

	}

	public ServiceFeedbackForm saveData(ServiceFeedbackForm feedbackForm) throws Exception {
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(feedbackForm.getCustomerid());
		if (findById.isPresent()) {
			ServiceFeedbackForm save = feedbackreposrepo.save(feedbackForm);
			return save;
		} else {
			throw new Exception("Record not Found");
		}

	}

	public FeedbackForm getFeedbacks(int customerid) {
		FeedbackForm feedbackrepos = feedbackrepo.findbycustomerid(customerid);
		return feedbackrepos;
	}

	public ServiceFeedbackForm getFeedback(int customerid) {
		ServiceFeedbackForm feedbackrepos = feedbackreposrepo.findbycustomerid(customerid);
		return feedbackrepos;
	}

	public List<OfflineCustomerDetails> getallofflinecustomer() {
		List<OfflineCustomerDetails> findById = offlinecustomerdetailsrepo.findAll();
		return findById;
	}

	public List<ExcelSheetToDBProductDetails> getData(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = repository.findAllByproductidIn(productid);
		for (int i = 0; i < findAllByProductIdIn.size(); i++) {
			int frequency = Collections.frequency(productid, findAllByProductIdIn.get(i).getProductid());
			findAllByProductIdIn.get(i).setQuantity(frequency);
			int quantity = findAllByProductIdIn.get(i).getQuantity();
			int purchaseamount = findAllByProductIdIn.get(i).getPurchaseamount();
			findAllByProductIdIn.get(i).setTotalamount(quantity * purchaseamount);
		}
		return findAllByProductIdIn;
	}

	public List<ExcelSheetToDBProductDetails> getListOfData(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = repository.findAllByproductidIn(productid);
		return findAllByProductIdIn;
	}

	public static String dateofexpiry(String dateofdelivery, String warrantydays) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, Integer.parseInt(warrantydays));
		String output = sdf.format(c.getTime());
		return output;
	}

	public List<ExcelSheetToDBProductDetails> getDetailsOfProduct(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> arrayList = new ArrayList<ExcelSheetToDBProductDetails>();
		for (int prodid : productid) {
			Optional<ExcelSheetToDBProductDetails> findById = productDetailsDataRepo.findById(prodid);
			if (findById.isPresent()) {
				arrayList.add(findById.get());
			}
		}
		System.out.println(arrayList);
		return arrayList;
	}

	public double getTotalSumOfTheProduct(List<Integer> productid) {
		double sum = 0;
		List<Integer> findpricebyproductid = repository.findpricebyproductid(productid);
		for (int i = 0; i < findpricebyproductid.size(); i++) {
			sum += findpricebyproductid.get(i);
		}
		return sum;
	}

	public double getTotalSumOfTheProducts(List<Integer> productid) {
		int cost = 0;
		List<ExcelSheetToDBProductDetails> data = getData(productid);
		for (int i = 0; i < data.size(); i++) {
			int totalamount = data.get(i).getTotalamount();
			cost += totalamount;
		}
		return cost;
	}

	public CustomerWarrantyResponse getDetails(CustomerWarrantyRequest customerWarrantyRequest) throws Exception {
		CustomerWarrantyResponse customerWarrantyResponse = new CustomerWarrantyResponse();
		Optional<OnlineCustomerRequest> customer = customerProductSaveRepo
				.findById(customerWarrantyRequest.getCustomerid());
		LocalDate purchasedDate = LocalDate.parse(customerWarrantyRequest.getDeliverydate());
		LocalDate warrantyDate = purchasedDate.plusDays(180);
		LocalDate now = LocalDate.now();
		Long range = ChronoUnit.DAYS.between(now, warrantyDate);
		if (now.compareTo(warrantyDate) < 0) {
			customerWarrantyResponse.setStatus("200");
			customerWarrantyResponse.setMessage("Days left for expiry " + range);
			customerWarrantyResponse.setWarrantyapplicable(true);
		} else {
			customerWarrantyResponse.setStatus("500");
			customerWarrantyResponse.setMessage("Warranty has been expired" + " " + range + " " + "Days ago");
			customerWarrantyResponse.setWarrantyapplicable(false);
		}
		return customerWarrantyResponse;
	}

	public ByteArrayInputStream load() {
		List<ExcelSheetToDBProductDetails> tutorials = repository.findAll();
		ByteArrayInputStream in = ExcelHelper.tutorialsToExcel(tutorials);
		return in;
	}

	public List<ExcelSheetToDBProductDetails> getAllTutorials() {
		return repository.findAll();
	}

	public OfflineCustomerResponse findofflinecustomerbyinvoiceandmobileno(int invoiceno, long mobileno) {
		OfflineCustomerResponse offlineCustomerResponse = new OfflineCustomerResponse();
		Optional<OfflineCustomerDetails> findbyinvoiceandmobile = offlinecustomerdetailsrepo
				.findbyinvoiceandmobile(invoiceno, mobileno);
		if (findbyinvoiceandmobile.isPresent()) {
			offlineCustomerResponse.setMessage("Data Retrieved");
			offlineCustomerResponse.setStatus("200");
			offlineCustomerResponse.setOfflineCustomerDetails(findbyinvoiceandmobile.get());
		} else {
			offlineCustomerResponse.setMessage("Data not Available");
			offlineCustomerResponse.setStatus("500");
		}
		return offlineCustomerResponse;
	}

}
