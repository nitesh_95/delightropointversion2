package com.bhushan.spring.files.excel.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.StockProduct;
import com.bhushan.spring.files.excel.model.InwardStock;
import com.bhushan.spring.files.excel.model.ProductPurchased;
import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.repository.AddProductInStockRepo;
import com.bhushan.spring.files.excel.repository.InwardStockRepo;
import com.bhushan.spring.files.excel.repository.ProductNameRepo;
import com.bhushan.spring.files.excel.repository.ProductPurchasedRepo;
import com.bhushan.spring.files.excel.response.ProdResponse;

@Service
public class StockProductService {

	@Autowired
	private AddProductInStockRepo addProductInStockRepo;

	@Autowired
	private ProductPurchasedRepo productpurchaserepo;
	@Autowired
	private ProductNameRepo productnamerepo;

	@Autowired
	private InwardStockRepo inwardstockrepo;

	public StockProduct saveData(StockProduct addProductInStock) {
		addProductInStock.setItemimage(".assets/img/"+addProductInStock.getItemname());
		StockProduct addproductinstock = addProductInStockRepo.save(addProductInStock);
		return addproductinstock;
	}

	public HashMap<String, Integer> getproductpurchasedlists() {
		int quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		List<Productnames> getbydate = getproducts();
		for (int i = 0; i < getbydate.size(); i++) {
			hashMap.put(getbydate.get(i).getProductname(), getbydate.get(i).getQuantity());
		}
		for (int i = 0; i < getbydate.size() - 1; i++) {
			for (int k = i + 1; k < getbydate.size(); k++) {
				if (getbydate.get(i).getProductname().equalsIgnoreCase(getbydate.get(k).getProductname())) {
					int collectx = getbydate.get(i).getQuantity();
					int collects = getbydate.get(k).getQuantity();
					quantities = collects + collectx;
					hashMap.put(getbydate.get(i).getProductname(), quantities);
				}
			}
		}
		return hashMap;
	}

	public HashMap<String, Integer> minusstockdata() {
		int quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		List<InwardStock> getallinwardstock = getallinwardstock();
		List<InwardStock> inwardstock = getallinwardstock();
		HashMap<String, Integer> getproductpurchasedlists = getproductpurchasedlists();
		for (int i = 0; i < inwardstock.size(); i++) {
			hashMap.put(inwardstock.get(i).getAddProductInStock().getItemname(), inwardstock.get(i).getQuantity());
		}
		return hashMap;
	}

	public List<Entry<String, Integer>> getstockproductdata() {
		int count = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		HashMap<String, Integer> getproductpurchasedlists = getproductpurchasedlists();
		HashMap<String, Integer> minusstockdata = minusstockdata();
		for (String key : getproductpurchasedlists.keySet()) {
			for (String keys : minusstockdata.keySet()) {
				if (key.equalsIgnoreCase(keys)) {
					Integer integer = getproductpurchasedlists.get(key);
					Integer integer2 = minusstockdata.get(keys);
					count = integer2 - integer;
					hashMap.put(key, count);
				}
			}
		}
		  List<Entry<String, Integer>> entryCustomerList = hashMap.entrySet()
                  .stream()
                  .collect(Collectors.toList());
		return entryCustomerList;
	}

	public InwardStock saveInwardStock(InwardStock inwardStock) {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		inwardStock.setDateofpurchase(timeStamp);
		double value = inwardStock.getUnitprice() * inwardStock.getQuantity();
		inwardStock.setInventory(value + value * inwardStock.getTaxrate() / 100);
		InwardStock addproductinstock = inwardstockrepo.save(inwardStock);
		return addproductinstock;
	}

	public ProdResponse deleteData(int id) {
		ProdResponse prodResponse = new ProdResponse();
		Optional<StockProduct> findById = addProductInStockRepo.findById(id);
		if (findById.isPresent()) {
			addProductInStockRepo.deleteById(id);
			prodResponse.setMessage("ProductDeleted");
			prodResponse.setStatus("200");
		} else {
			prodResponse.setStatus("500");
			prodResponse.setMessage("ProductNotAvailable");
		}
		return prodResponse;
	}

	public List<InwardStock> getallinwardstock() {
		List<InwardStock> findAll = inwardstockrepo.findAll();
		for (int i = 0; i < findAll.size(); i++) {
			findAll.get(i).setRemainingstock(1);
		}
		return findAll;
	}

	public List<StockProduct> getallstockproductname() {
		return addProductInStockRepo.findAll();
	}

	public List<Productnames> getproducts() {
		return productnamerepo.findAll();
	}

	public List<ProductPurchased> getproductsdata() {
		return productpurchaserepo.findAll();
	}

	public List<Productnames> getbydate(String dateofpurchase) {
		return productnamerepo.getbydate(dateofpurchase);
	}

	public List<ProductPurchased> getbydates(String dateofpurchase) {
		int quanitites = 0;
		List<ProductPurchased> getbydates = productpurchaserepo.getbydates(dateofpurchase);
		for (int i = 0; i < getbydates.size() - 1; i++)
			for (int k = i + 1; k < getbydates.size(); k++)
				if (getbydates.get(i).getProductnamesdata() == getbydates.get(k).getProductnamesdata()) {
					int collectx = getbydates.get(i).getQuantities();
					int collects = getbydates.get(k).getQuantities();
					quanitites = collects + collectx;
				}
		System.out.println(quanitites);
		return getbydates;
	}
}
