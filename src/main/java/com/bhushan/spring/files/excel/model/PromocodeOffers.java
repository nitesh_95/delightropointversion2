package com.bhushan.spring.files.excel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "codeoffer")
public class PromocodeOffers {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String promocode;
	private String promocodegenerationdate;
	private double promocodevalue;
	private boolean promocodeused;
	private Long mobileno;
	

	public Long getMobileno() {
		return mobileno;
	}

	public void setMobileno(Long mobileno) {
		this.mobileno = mobileno;
	}

	public String getPromocodegenerationdate() {
		return promocodegenerationdate;
	}

	public void setPromocodegenerationdate(String promocodegenerationdate) {
		this.promocodegenerationdate = promocodegenerationdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPromocode() {
		return promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

	public double getPromocodevalue() {
		return promocodevalue;
	}

	public void setPromocodevalue(double promocodevalue) {
		this.promocodevalue = promocodevalue;
	}

	public boolean isPromocodeused() {
		return promocodeused;
	}

	public void setPromocodeused(boolean promocodeused) {
		this.promocodeused = promocodeused;
	}

}
