package com.bhushan.spring.files.excel.controller;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.ServicingEntity;
import com.bhushan.spring.files.excel.response.ServicingEntityResponsee;
import com.bhushan.spring.files.excel.service.Servicingdetailsservice;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class ServicingController {
	@Autowired
	private Servicingdetailsservice servicingdetailsservice;

	@PostMapping("/servicingdetails")
	public ServicingEntity saveServicingdetails(@RequestBody ServicingEntity addProductInStock)
			throws ClientProtocolException, IOException {
		ServicingEntity saveData = servicingdetailsservice.savedata(addProductInStock);
		return saveData;
	}

	
	@PostMapping("/saveonlineservicingdetails/{invoiceno}/{mobileno}")
	public ServicingEntity saveonlineservicingdetails(@RequestBody ServicingEntity servicingEntity, @PathVariable int invoiceno, @PathVariable long mobileno)
			throws Exception {
		ServicingEntity saveData = servicingdetailsservice.saveonlineservicingdetails(servicingEntity , invoiceno , mobileno);
		return saveData;
	}

	
	@GetMapping("/getbyticketnoandmobileno/{ticketno}/{mobileno}")
	public ServicingEntityResponsee getbyticketnoandmobileno(@PathVariable String ticketno,
			@PathVariable Long mobileno) {
		ServicingEntityResponsee findbyticketnoandmobileno = servicingdetailsservice.getbyticketnoandmobileno(ticketno,
				mobileno);
		return findbyticketnoandmobileno;
	}

	@PostMapping("/gettotalamount")
	public double gettotalamount(@RequestBody ServicingEntity addProductInStock) {
		return servicingdetailsservice.gettotalamount(addProductInStock);
	}

	@GetMapping("/servicingdetailslist")
	public List<ServicingEntity> findallstock() {
		return servicingdetailsservice.findall();
	}

	@GetMapping("/servicingdetailslist/{id}")
	public ServicingEntity deleteData(@PathVariable int id) throws Exception {
		return servicingdetailsservice.findbyid(id);
	}

	@GetMapping("/resendotp/{ticketno}/{mobileno}")
	public ServicingEntityResponsee resendOtp(@PathVariable String ticketno, @PathVariable Long mobileno)
			throws ClientProtocolException, IOException {
		return servicingdetailsservice.resendOtp(ticketno, mobileno);
	}

	@GetMapping("/verifyOtp/{otp}/{ticketno}/{mobileno}")
	public ServicingEntityResponsee resendOtp(@PathVariable String otp, @PathVariable String ticketno,
			@PathVariable Long mobileno) throws ClientProtocolException, IOException {
		return servicingdetailsservice.otpverification(otp, ticketno, mobileno);
	}
}