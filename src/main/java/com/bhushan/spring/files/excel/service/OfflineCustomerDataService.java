package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.Emipayment;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.model.PromocodeOffers;
import com.bhushan.spring.files.excel.model.ReturnProductName;
import com.bhushan.spring.files.excel.repository.OfflineCustomerDetailsRepo;
import com.bhushan.spring.files.excel.repository.PromocodeOfferRepo;
import com.bhushan.spring.files.excel.response.OfflinCustomerDetailsResponse;
import com.bhushan.spring.files.excel.response.RefundCalculationRequest;
import com.bhushan.spring.files.excel.response.RefundCalculationRequestOffline;
import com.bhushan.spring.files.excel.response.RefundCalculationRequestOfflineDetails;
import com.bhushan.spring.files.excel.response.ReturnProductNames;

@Service
public class OfflineCustomerDataService {

	@Autowired
	private OfflineCustomerDetailsRepo offlineCustomerDetailsRepo;
	@Autowired
	private MessageUtil messageUtil;
	@Autowired
	private PromocodeOfferRepo promocodeOfferRepo;

	public OfflinCustomerDetailsResponse saveOfflineCustomerDetails(OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		String generatedString = RandomStringUtils.randomNumeric(6);
		PromocodeOffers promocodeOffers = new PromocodeOffers();
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		OfflinCustomerDetailsResponse offlinCustomerDetailsResponse = new OfflinCustomerDetailsResponse();
		response.setName(response.getName());
		response.setMobileno(response.getMobileno());
		response.setInvoiceno(generatedString);
		response.setMailid(response.getMailid());
		response.setDateofpurchase(timeStamp);
		response.setAddress(response.getAddress());
		JSONObject createJson = new JSONObject(response);
		org.json.JSONArray configArray = createJson.getJSONArray("productPurchaseds");
		double sumData = 0;
		ArrayList<Double> arrayList2 = new ArrayList<>();
		for (int i = 0; i < response.getProductPurchaseds().size(); i++) {
			response.getProductPurchaseds().get(i).setCount(i + 1);
			response.getProductPurchaseds().get(i).setTypeofcustomer("OFFLINE_CUSTOMER");
			response.getProductPurchaseds().get(i).setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames()
					.setQuantity(response.getProductPurchaseds().get(i).getProductnames().getQuantity());
			double quantity = response.getProductPurchaseds().get(i).getQuantities();
			double purchaseamount = response.getProductPurchaseds().get(i).getUnitprices();
			response.getProductPurchaseds().get(i).getProductnames().setTotalamount(quantity * purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setUnitprice(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setDescription(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
			response.getProductPurchaseds().get(i).getProductnames()
					.setImagepath(response.getProductPurchaseds().get(i).getProductnames().getImagepath());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductid(response.getProductPurchaseds().get(i).getProductnames().getProductid());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductname(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).getProductnames()
					.setPurchaseamount(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			int value = (int) quantity;
			response.getProductPurchaseds().get(i).getProductnames().setQuantity(value);
			arrayList2.add(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setTotalamount(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames().setUnitprice(purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setWarrantydays(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
		}
		double sum = 0;
		for (Double integer : arrayList2) {
			sum += integer;
		}
		response.getBillingdetails().setFullamount(sum);
		double totaltaxrate = response.getBillingdetails().getGstrate()
				+ response.getBillingdetails().getOtherTaxRate();
		double amount = response.getBillingdetails().getFullamount() * totaltaxrate / 100;
		double totalamount = amount + response.getBillingdetails().getFullamount();
		response.getBillingdetails().setGstrate(response.getBillingdetails().getGstrate());
		response.getBillingdetails().setOtherTaxRate(response.getBillingdetails().getOtherTaxRate());
		response.getBillingdetails().setOthercharges(response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setEntirerate(totalamount + response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setPaidamount(response.getBillingdetails().getPaidamount());
		if (response.getBillingdetails().getPromocodeapplieds() != null) {
			Optional<PromocodeOffers> findbyPromoCode = promocodeOfferRepo
					.findbyPromoCode(response.getBillingdetails().getPromocodeapplieds());
			if (findbyPromoCode.isPresent()) {
				promocodeOffers.setId(findbyPromoCode.get().getId());
				promocodeOffers.setPromocode(findbyPromoCode.get().getPromocode());
				promocodeOffers.setPromocodeused(true);
				promocodeOffers.setPromocodevalue(findbyPromoCode.get().getPromocodevalue());
				promocodeOffers.setPromocodegenerationdate(findbyPromoCode.get().getPromocodegenerationdate());
				promocodeOffers.setMobileno(findbyPromoCode.get().getMobileno());
				promocodeOfferRepo.save(promocodeOffers);
			}
		}
		response.getBillingdetails().setRemainingamount(response.getBillingdetails().getEntirerate()
				- response.getBillingdetails().getPaidamount() - response.getBillingdetails().getPromocodevalues());
		response.getBillingdetails().setEmiapplicable(response.getBillingdetails().isEmiapplicable());
		double sumDatas = 0;
		try {
			double remainingamount = response.getBillingdetails().getRemainingamount();
			double otherTaxRate = response.getBillingdetails().getOtherTaxRate();
			int noofmonths = response.getBillingdetails().getEmidata().getNoofmonths();
			double calculateCompoundInts = calculateCompoundInt(remainingamount, noofmonths, otherTaxRate, 1);

			response.getBillingdetails().getEmidata().setMontlyinstallment(
					calculateCompoundInts / response.getBillingdetails().getEmidata().getNoofmonths());
			response.getBillingdetails().getEmidata()
					.setNoofmonths((response.getBillingdetails().getEmidata().getNoofmonths()));
			response.getBillingdetails().setEmicleared(response.getBillingdetails().getEmidata().getEmiDatas()
					.get(response.getBillingdetails().getEmidata().getEmiDatas().size() - 1).isEmipaymentEnded());

			List<Emipayment> emiDatas = response.getBillingdetails().getEmidata().getEmiDatas();
			ArrayList<Double> arrayList3 = new ArrayList<>();
			for (int i = 0; i < emiDatas.size(); i++) {
				double paidamount = response.getBillingdetails().getEmidata().getEmiDatas().get(i).getPaidamount();
				arrayList3.add(paidamount);
			}
			for (Double integer : arrayList3) {
				sumDatas += integer;
			}
			response.getBillingdetails().getEmidata().getEmiDatas().get(0)
					.setAmount(response.getBillingdetails().getRemainingamount());
			response.getBillingdetails().getEmidata().getEmiDatas().get(0)
					.setDate(response.getBillingdetails().getEmidata().getEmiDatas().get(0).getDate());
			response.getBillingdetails().getEmidata().getEmiDatas().get(0)
					.setRemainingbalance(response.getBillingdetails().getRemainingamount() - arrayList3.get(0));
			for (int i = 1; i <= response.getBillingdetails().getEmidata().getEmiDatas().size() - 1; i++) {
				response.getBillingdetails().getEmidata().getEmiDatas().get(i).setAmount(
						response.getBillingdetails().getEmidata().getEmiDatas().get(i - 1).getRemainingbalance());
				response.getBillingdetails().getEmidata().getEmiDatas().get(i).setRemainingbalance(
						response.getBillingdetails().getEmidata().getEmiDatas().get(i - 1).getRemainingbalance()
								- arrayList3.get(i));
				if (response.getBillingdetails().getEmidata().getEmiDatas().get(i).getRemainingbalance() == 0) {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i).setEmipaymentEnded(true);
				} else {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i).setEmipaymentEnded(false);
				}
				if (response.getBillingdetails().getEmidata().getEmiDatas().get(i).getDate() == null) {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i)
							.setDate(response.getDateofpurchase());
				} else {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i)
							.setDate(response.getBillingdetails().getEmidata().getEmiDatas().get(i).getDate());
				}
			}
		} catch (Exception e) {
			response.getBillingdetails().setEmidata(null);
		}

		boolean emipaymentEnded = response.getBillingdetails().getEmidata().getEmiDatas()
				.get(response.getBillingdetails().getEmidata().getEmiDatas().size() - 1).isEmipaymentEnded();
		System.out.println("EMI PAYMENT ENDED" + emipaymentEnded);
		response.getBillingdetails().setEmicleared(emipaymentEnded);
		response.getBillingdetails().getEmidata()
				.setRemainingamount(response.getBillingdetails().getRemainingamount() - sumDatas);
		response.getBillingdetails().getEmidata().setEmiDatas(response.getBillingdetails().getEmidata().getEmiDatas());
		OfflineCustomerDetails save = offlineCustomerDetailsRepo.save(response);
		messageUtil.sendInfoForOnlineCustomer(save);
		offlinCustomerDetailsResponse.setOfflinecustomerid(save.getOfflinecustomerid());
		offlinCustomerDetailsResponse.setName(save.getName());
		offlinCustomerDetailsResponse.setMobileno(save.getMobileno());
		offlinCustomerDetailsResponse.setMailid(save.getMailid());
		offlinCustomerDetailsResponse.setInvoiceno(generatedString);
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setProductPurchaseds(save.getProductPurchaseds());
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setDateofpurchase(save.getDateofpurchase());
		return offlinCustomerDetailsResponse;
	}

	public OfflinCustomerDetailsResponse updateData(OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		String generatedString = RandomStringUtils.randomNumeric(6);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		OfflinCustomerDetailsResponse offlinCustomerDetailsResponse = new OfflinCustomerDetailsResponse();
		response.setName(response.getName());
		response.setMobileno(response.getMobileno());
		response.setInvoiceno(generatedString);
		response.setMailid(response.getMailid());
		response.setDateofpurchase(timeStamp);
		response.setAddress(response.getAddress());
		if (response.getBillingdetails().getEmidata() != null) {
			HashMap<String, HashMap<Double, Double>> interest = getInterest(
					response.getBillingdetails().getOtherTaxRate(),
					response.getBillingdetails().getEmidata().getNoofmonths(),
					response.getBillingdetails().getRemainingamount());
			response.setGetdetails(interest);
		} else {
			response.getBillingdetails().setEmidata(null);
		}
		ArrayList<Double> arrayList2 = new ArrayList<>();
		for (int i = 0; i < response.getProductPurchaseds().size(); i++) {
			response.getProductPurchaseds().get(i)
					.setProductnamesdata(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).setCount(i);
			response.getProductPurchaseds().get(i).getProductnames()
					.setQuantity(response.getProductPurchaseds().get(i).getProductnames().getQuantity());
			response.getProductPurchaseds().get(i).setTypeofcustomer("OFFLINE_CUSTOMER");
			response.getProductPurchaseds().get(i).setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setTypeofcustomer("OFFLINE_CUSTOMER");
			double quantity = response.getProductPurchaseds().get(i).getQuantities();
			double purchaseamount = response.getProductPurchaseds().get(i).getUnitprices();
			response.getProductPurchaseds().get(i).getProductnames().setTotalamount(quantity * purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setUnitprice(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setDescription(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
			response.getProductPurchaseds().get(i).getProductnames()
					.setImagepath(response.getProductPurchaseds().get(i).getProductnames().getImagepath());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductid(response.getProductPurchaseds().get(i).getProductnames().getProductid());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductname(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).getProductnames()
					.setPurchaseamount(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			int value = (int) quantity;
			response.getProductPurchaseds().get(i).getProductnames().setQuantity(value);
			arrayList2.add(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setTotalamount(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames().setUnitprice(purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setWarrantydays(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
		}
		double sum = 0;
		for (Double integer : arrayList2) {
			sum += integer;
		}
		response.getBillingdetails().setFullamount(sum);
		double totaltaxrate = response.getBillingdetails().getGstrate()
				+ response.getBillingdetails().getOtherTaxRate();
		double amount = response.getBillingdetails().getFullamount() * totaltaxrate / 100;
		double totalamount = amount + response.getBillingdetails().getFullamount();
		response.getBillingdetails().setGstrate(response.getBillingdetails().getGstrate());
		response.getBillingdetails().setOtherTaxRate(response.getBillingdetails().getOtherTaxRate());
		response.getBillingdetails().setOthercharges(response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setEntirerate(totalamount + response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setPaidamount(response.getBillingdetails().getPaidamount());
		response.getBillingdetails().setRemainingamount(
				response.getBillingdetails().getEntirerate() - response.getBillingdetails().getPaidamount());
		double remainingamount = response.getBillingdetails().getRemainingamount();
		double otherTaxRate = response.getBillingdetails().getOtherTaxRate();
		if (response.getBillingdetails().getEmidata() != null) {
			int noofmonths = response.getBillingdetails().getEmidata().getNoofmonths();
			double calculateCompoundInt = calculateCompoundInt(remainingamount, noofmonths, otherTaxRate, 4);
			response.getBillingdetails().getEmidata().setMontlyinstallment(calculateCompoundInt);
			response.getBillingdetails().setEmiapplicable(response.getBillingdetails().isEmiapplicable());
		} else {
			response.getBillingdetails().setEmidata(null);
		}

		double sumDatas = 0;
		ArrayList<Double> arrayList3 = new ArrayList<>();
		for (Double integer : arrayList3) {
			sumDatas += integer;
		}

		OfflineCustomerDetails save = offlineCustomerDetailsRepo.save(response);
		messageUtil.sendInfoForOnlineCustomer(save);
		offlinCustomerDetailsResponse.setOfflinecustomerid(save.getOfflinecustomerid());
		offlinCustomerDetailsResponse.setName(save.getName());
		offlinCustomerDetailsResponse.setMobileno(save.getMobileno());
		offlinCustomerDetailsResponse.setMailid(save.getMailid());
		offlinCustomerDetailsResponse.setInvoiceno(generatedString);
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setProductPurchaseds(save.getProductPurchaseds());
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setDateofpurchase(save.getDateofpurchase());
		return offlinCustomerDetailsResponse;
	}

	public static double getdetails(double rate, double time, double principal) {
		double multiplier = Math.pow(1.0 + rate / 100.0, time) - 1.0;
		return multiplier * principal;
	}

	public HashMap<String, HashMap<Double, Double>> getInterest(double rate, double time, double principal) {
		int months[] = { 3, 6, 9, 12 };
		HashMap<Double, Double> hashMap = new HashMap<>();
		HashMap<String, HashMap<Double, Double>> response = new HashMap<>();
		for (double mon : months) {
			double value = principal + getdetails(rate, mon, principal);
			hashMap.put(mon, value);
			response.put("Months and their Interest Rate", hashMap);
		}
		return response;
	}

	public double calculateCompoundInt(double p, int t, double r, double n) {
		double amount = p * Math.pow(1 + (r / n), n * t);
		double cinterest = amount - p;
		return p + cinterest;
	}

	public double refundmoney(RefundCalculationRequestOfflineDetails refundCalcuationRequest) {
		double sum = 0;
		for (ReturnProductNames productname : refundCalcuationRequest.getReturnedproduct()) {
			double totalamount = productname.getQuantities() * productname.getUnitprices();
			sum = sum + totalamount;
		}
		return sum;
	}

	public Optional<OfflineCustomerDetails> updateDatas(RefundCalculationRequestOfflineDetails response)
			throws Exception {
		double sum = 0;
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		Optional<OfflineCustomerDetails> findById = offlineCustomerDetailsRepo.findById(response.getId());

		if (findById.isPresent()) {
			OfflineCustomerDetails onlineCustomerRequest = new OfflineCustomerDetails();
			onlineCustomerRequest.setOfflinecustomerid(findById.get().getOfflinecustomerid());
			onlineCustomerRequest.setDateofpurchase(findById.get().getDateofpurchase());
			onlineCustomerRequest.setName(findById.get().getName());
			onlineCustomerRequest.setMailid(findById.get().getMailid());
			onlineCustomerRequest.setAddress(findById.get().getAddress());
			onlineCustomerRequest.setMobileno(findById.get().getMobileno());
			onlineCustomerRequest.setInvoiceno(findById.get().getInvoiceno());

			for (int i = 0; i < response.getReturnedproduct().size(); i++) {

				response.getReturnedproduct().get(i).setDateofpurchase(findById.get().getDateofpurchase());
				response.getReturnedproduct().get(i)
						.setProductnames(findById.get().getProductPurchaseds().get(i).getProductnames());
				response.getReturnedproduct().get(i)
						.setQuantities(findById.get().getProductPurchaseds().get(i).getQuantities());
				response.getReturnedproduct().get(i)
						.setUnitprices(findById.get().getProductPurchaseds().get(i).getUnitprices());
				response.getReturnedproduct().get(i).setTypeofcustomer("Offline Customer");
				response.getReturnedproduct().get(i)
						.setPurchaseid(findById.get().getProductPurchaseds().get(i).getPurchaseid());
				response.getReturnedproduct().get(i)
						.setProductnamesdata(findById.get().getProductPurchaseds().get(i).getProductnamesdata());
				response.getReturnedproduct().get(i)
						.setProductnames(findById.get().getProductPurchaseds().get(i).getProductnames());
				sum = sum + findById.get().getProductPurchaseds().get(i).getQuantities()
						+ findById.get().getProductPurchaseds().get(i).getUnitprices();
			}
			onlineCustomerRequest.setProductPurchaseds(findById.get().getProductPurchaseds());
			onlineCustomerRequest.setBillingdetails(findById.get().getBillingdetails());
			onlineCustomerRequest.setReturnproductname(null);
			onlineCustomerRequest.setReturnproductname(response.getReturnedproduct());
			String promocode = RandomStringUtils.randomAlphanumeric(7).toUpperCase();

			PromocodeOffers offer = new PromocodeOffers();
			offer.setPromocode(promocode);
			offer.setPromocodeused(false);
			offer.setMobileno(onlineCustomerRequest.getMobileno());
			offer.setPromocodegenerationdate(timeStamp);
			offer.setPromocodevalue(refundmoney(response));
			onlineCustomerRequest.setPromocodeoffer(offer);
			OfflineCustomerDetails save = offlineCustomerDetailsRepo.save(onlineCustomerRequest);
			if (save.getReturnproductname() != null && !save.getReturnproductname().isEmpty()) {
				messageUtil.returnProductoffline(save);
			}
			return offlineCustomerDetailsRepo.findById(save.getOfflinecustomerid());
		} else {
			throw new Exception("The Id is not Present");
		}
	}
}
