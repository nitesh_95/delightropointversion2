package com.bhushan.spring.files.excel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.Productnames;

@Repository
public interface ProductNameRepo extends JpaRepository<Productnames, Integer> {

	@Query(value = "select * from productnames where dateofpurchase =?1", nativeQuery = true)
	List<Productnames> getbydate(@Param(value = "dateofpurchase") String dateofpurchase);

}
