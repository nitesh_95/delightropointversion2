package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.PromocodeOffers;

public class PromocodeOfferResponse {

	private String status;
	private String message;
	private PromocodeOffers promocodeOffers;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PromocodeOffers getPromocodeOffers() {
		return promocodeOffers;
	}

	public void setPromocodeOffers(PromocodeOffers promocodeOffers) {
		this.promocodeOffers = promocodeOffers;
	}

}
