package com.bhushan.spring.files.excel.response;

import java.util.List;

import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;

public class AddProductViewResponse {

	private List<ExcelSheetToDBProductDetails> addingProductForViews;
	private List<ProductFrequency> productFrequencies;

	public List<ExcelSheetToDBProductDetails> getAddingProductForViews() {
		return addingProductForViews;
	}

	public void setAddingProductForViews(List<ExcelSheetToDBProductDetails> addingProductForViews) {
		this.addingProductForViews = addingProductForViews;
	}

	public List<ProductFrequency> getProductFrequencies() {
		return productFrequencies;
	}

	public void setProductFrequencies(List<ProductFrequency> productFrequencies) {
		this.productFrequencies = productFrequencies;
	}

}
