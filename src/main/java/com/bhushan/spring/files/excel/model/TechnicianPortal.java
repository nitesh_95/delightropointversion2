package com.bhushan.spring.files.excel.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "technicianportal")
public class TechnicianPortal {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int customerids;
	private String customertype;
	private String name;
	private String mailid;
	private String address;
	private Long mobileno;
	private String productname;
	private String dateofpurchase;
	private String dateofservicerequest;
	private String assignedto;
	private boolean iswarranty;
	private double charge;
	private double techniciancost;
	private float tax;
	private double totalcost;
	private double paidamount;
	private String ticketno;
	private String dateofresolvingissue;
	private double remainingamount;
	private String ticketstatus;
	private String invoiceno;
	private double paidservicingamount;
	private String ticketstatuss;
	@Column(name = "issuedescription", nullable = false, length = 500)
	private String issuedescription;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "customerids")
	private List<ProductPurchased> productPurchaseds;


	public String getTicketstatuss() {
		return ticketstatuss;
	}

	public void setTicketstatuss(String ticketstatuss) {
		this.ticketstatuss = ticketstatuss;
	}

	public int getCustomerids() {
		return customerids;
	}

	public void setCustomerids(int customerids) {
		this.customerids = customerids;
	}

	public String getCustomertype() {
		return customertype;
	}

	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getMobileno() {
		return mobileno;
	}

	public void setMobileno(Long mobileno) {
		this.mobileno = mobileno;
	}


	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public String getDateofservicerequest() {
		return dateofservicerequest;
	}

	public void setDateofservicerequest(String dateofservicerequest) {
		this.dateofservicerequest = dateofservicerequest;
	}

	public String getAssignedto() {
		return assignedto;
	}

	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public boolean isIswarranty() {
		return iswarranty;
	}

	public void setIswarranty(boolean iswarranty) {
		this.iswarranty = iswarranty;
	}

	public double getCharge() {
		return charge;
	}

	public void setCharge(double charge) {
		this.charge = charge;
	}

	public double getTechniciancost() {
		return techniciancost;
	}

	public void setTechniciancost(double techniciancost) {
		this.techniciancost = techniciancost;
	}

	public float getTax() {
		return tax;
	}

	public void setTax(float tax) {
		this.tax = tax;
	}

	public double getTotalcost() {
		return totalcost;
	}

	public void setTotalcost(double totalcost) {
		this.totalcost = totalcost;
	}

	public double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(double paidamount) {
		this.paidamount = paidamount;
	}

	public String getTicketno() {
		return ticketno;
	}

	public void setTicketno(String ticketno) {
		this.ticketno = ticketno;
	}

	public String getDateofresolvingissue() {
		return dateofresolvingissue;
	}

	public void setDateofresolvingissue(String dateofresolvingissue) {
		this.dateofresolvingissue = dateofresolvingissue;
	}

	public double getRemainingamount() {
		return remainingamount;
	}

	public void setRemainingamount(double remainingamount) {
		this.remainingamount = remainingamount;
	}

	public String getTicketstatus() {
		return ticketstatus;
	}

	public void setTicketstatus(String ticketstatus) {
		this.ticketstatus = ticketstatus;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public double getPaidservicingamount() {
		return paidservicingamount;
	}

	public void setPaidservicingamount(double paidservicingamount) {
		this.paidservicingamount = paidservicingamount;
	}

	public String getIssuedescription() {
		return issuedescription;
	}

	public void setIssuedescription(String issuedescription) {
		this.issuedescription = issuedescription;
	}

	public List<ProductPurchased> getProductPurchaseds() {
		return productPurchaseds;
	}

	public void setProductPurchaseds(List<ProductPurchased> productPurchaseds) {
		this.productPurchaseds = productPurchaseds;
	}

}