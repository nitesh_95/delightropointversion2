package com.bhushan.spring.files.excel.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.repository.ReturnOnlineProduct;
import com.bhushan.spring.files.excel.response.ReturnOnlineCustomerResponse;
import com.bhushan.spring.files.excel.service.OnlineCustomerService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class ReturnOnlineCustomer {
	
	@Autowired
	private OnlineCustomerService returnOnlineCustomerService;
	@Autowired
	private ReturnOnlineProduct returnOnlineProduct;

	@PostMapping("/saveReturnOnlineCustomer/{invoiceno1}/{mobileno}/{invoiceno2}")
	public ReturnOnlineCustomerResponse  updateReturnRequest( @PathVariable int invoiceno1, @PathVariable long mobileno ,@PathVariable int invoiceno2) throws Exception {
		return returnOnlineCustomerService.updateReturnRequest(invoiceno1,mobileno, invoiceno2);
		
	}
	@GetMapping("/fetchonlinereturncustomer/{id}")
	public Optional<com.bhushan.spring.files.excel.model.ReturnOnlineCustomer> updateReturnRequest(@PathVariable int id) throws Exception {
		return returnOnlineProduct.findById(id);
		
	}
}
