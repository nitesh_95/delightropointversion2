package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;

@Repository
public interface OfflineCustomerDetailsRepo extends JpaRepository<OfflineCustomerDetails, Integer>{

	@Query(value = "select * from offlinecustomerdetails where invoiceno=?1 and mobileno=?2", nativeQuery = true)
	Optional<OfflineCustomerDetails>  findbyinvoiceandmobile(@Param(value = "invoiceno") int invoiceno,
			@Param(value = "mobileno") long mobileno);
}
