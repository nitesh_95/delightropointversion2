package com.bhushan.spring.files.excel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;

public interface ProductRepository extends JpaRepository<ExcelSheetToDBProductDetails, Integer> {

	@Query(value = "select purchaseamount from productdetailsdata where productid=?1;", nativeQuery = true)
	double priceOfProduct(@Param("productid") Integer productid);
	
	List<ExcelSheetToDBProductDetails> findAllByproductidIn(List<Integer> productid);
	
	@Query(value = "select purchaseamount from productdetailsdata where productid in :productid",nativeQuery = true)
	List<Integer> findpricebyproductid(List<Integer> productid);
	
	
}
