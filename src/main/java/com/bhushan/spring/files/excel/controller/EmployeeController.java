package com.bhushan.spring.files.excel.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.bhushan.spring.files.excel.model.EmployeeEntity;
import com.bhushan.spring.files.excel.response.ProdResponse;
import com.bhushan.spring.files.excel.service.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class EmployeeController {

	@Autowired
	EmployeeService fileService;

	@PostMapping("/employeeentity")
	public Optional<EmployeeEntity> saveEmployeeEntity(@RequestBody EmployeeEntity employeeEntity)
			throws ClientProtocolException, IOException {
		return fileService.saveDatas(employeeEntity);
	}

	@GetMapping("/getDetailsdata")
	public List<EmployeeEntity> getall() throws Exception {
		return fileService.getallemployees();
	}

	@GetMapping("/getemployeename")
	public List<String> getemployeeName() throws Exception {
		return fileService.getallemployeesName();
	}
	@GetMapping("/getemployeebyid/{id}")
	public EmployeeEntity findbyemployeeids(@PathVariable int id) throws Exception {
		return fileService.findbyemployeeid(id);
	}

	@PostMapping("/deletemapping/{id}")
	public ProdResponse deletebyemployeeid(@PathVariable int id) throws Exception {
		return fileService.deleteemployee(id);
	}

}
