package com.bhushan.spring.files.excel.response;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RefundCalculationRequestOfflineDetails {
	private int id;
	private List<ReturnProductNames> returnedproduct;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public List<ReturnProductNames> getReturnedproduct() {
		return returnedproduct;
	}

	public void setReturnedproduct(List<ReturnProductNames> returnedproduct) {
		this.returnedproduct = returnedproduct;
	}

}
