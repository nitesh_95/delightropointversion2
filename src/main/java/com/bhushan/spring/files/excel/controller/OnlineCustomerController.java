package com.bhushan.spring.files.excel.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.PromocodeOffers;
import com.bhushan.spring.files.excel.response.OnlineCustomerResponse;
import com.bhushan.spring.files.excel.response.PromocodeOfferResponse;
import com.bhushan.spring.files.excel.response.RefundCalculationRequest;
import com.bhushan.spring.files.excel.response.Response;
import com.bhushan.spring.files.excel.service.ExcelService;
import com.bhushan.spring.files.excel.service.OnlineCustomerService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class OnlineCustomerController {

	@Autowired
	OnlineCustomerService onlineCustomerService;

	@Autowired
	ExcelService fileService;

	@GetMapping("/promocodevalue/{promocode}")
	public Double findbyPromocode(@PathVariable String promocode) {
		Double findbyPromoCode = onlineCustomerService.findbyPromocode(promocode);
		return findbyPromoCode;
	}

	@PostMapping("/disbursepromocode/{promocode}")
	public PromocodeOfferResponse disbursePromocode(@PathVariable String promocode) throws Exception {
		PromocodeOfferResponse disbursePromocode = onlineCustomerService.disbursePromocode(promocode);
		return disbursePromocode;
	}

	@PostMapping("/saveData")
	public Optional<OnlineCustomerRequest> saveData(@RequestBody OnlineCustomerRequest response)
			throws ClientProtocolException, IOException {
		System.out.println(response);
		return onlineCustomerService.saveData(response);
	}

	@GetMapping("/onlinecustomerid/{invoiceno}/{mobileno}")
	public OnlineCustomerResponse findonlinecustomer(@PathVariable int invoiceno, @PathVariable long mobileno) {
		OnlineCustomerResponse findonlinecustomerbyinvoiceandmobileno = onlineCustomerService
				.findonlinecustomerbyinvoiceandmobileno(invoiceno, mobileno);
		return findonlinecustomerbyinvoiceandmobileno;
	}

	@GetMapping("/onlinecustomerids/{invoiceno}/{mobileno}")
	public OnlineCustomerResponse getOnlineCustomer(@PathVariable int invoiceno, @PathVariable long mobileno) {
		OnlineCustomerResponse findonlinecustomerbyinvoiceandmobileno = onlineCustomerService
				.getOnlineCustomer(invoiceno, mobileno);
		return findonlinecustomerbyinvoiceandmobileno;
	}

	@GetMapping("/onlinecustomerid/{id}")
	public OnlineCustomerRequest findonlinecustomerbyid(@PathVariable int id) throws Exception {
		OnlineCustomerRequest findonlinecustomerbyinvoiceandmobileno = onlineCustomerService.findbyid(id);
		return findonlinecustomerbyinvoiceandmobileno;
	}

	@GetMapping("/getCustomerOnlineCustomerList")
	public List<OnlineCustomerRequest> getListOfData() {
		List<OnlineCustomerRequest> findAll = onlineCustomerService.getListOfData();
		return findAll;
	}

	@PostMapping("/refundmoney")
	public double refundcalculation(@RequestBody RefundCalculationRequest refundcalculationrequest) {
		return onlineCustomerService.refundmoney(refundcalculationrequest);
	}

	@GetMapping("/getDetails/{id}")
	public OnlineCustomerRequest findbyids(@PathVariable int id) throws Exception {
		return onlineCustomerService.findbyids(id);
	}

	@PostMapping("/updatereturnproduct")
	public Optional<OnlineCustomerRequest> updatereturnproduct(@RequestBody RefundCalculationRequest calculationRequest)
			throws Exception {
		return onlineCustomerService.updateDatas(calculationRequest);
	}
}
