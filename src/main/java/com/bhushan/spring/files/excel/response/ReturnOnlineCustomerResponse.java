package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.ReturnOnlineCustomer;

public class ReturnOnlineCustomerResponse {

	private String message;
	private String status;
	private ReturnOnlineCustomer onlineCustomer;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ReturnOnlineCustomer getOnlineCustomer() {
		return onlineCustomer;
	}

	public void setOnlineCustomer(ReturnOnlineCustomer onlineCustomer) {
		this.onlineCustomer = onlineCustomer;
	}

}
