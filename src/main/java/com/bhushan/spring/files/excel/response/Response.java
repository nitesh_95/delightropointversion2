package com.bhushan.spring.files.excel.response;

import java.util.List;

import com.bhushan.spring.files.excel.model.CustomerProductSave;
import com.bhushan.spring.files.excel.model.PaymentHistory;
import com.bhushan.spring.files.excel.model.ProductPurchased;
import com.bhushan.spring.files.excel.model.PurchaseDetails;

public class Response {

	private CustomerProductSave customerProductSave;
	private PurchaseDetails purchasedetails;
	private List<ProductPurchased> productpurchased;
	private PaymentHistory paymenthistory;


	public CustomerProductSave getCustomerProductSave() {
		return customerProductSave;
	}

	public void setCustomerProductSave(CustomerProductSave customerProductSave) {
		this.customerProductSave = customerProductSave;
	}

	public PurchaseDetails getPurchasedetails() {
		return purchasedetails;
	}

	public void setPurchasedetails(PurchaseDetails purchasedetails) {
		this.purchasedetails = purchasedetails;
	}

	public List<ProductPurchased> getProductpurchased() {
		return productpurchased;
	}

	public void setProductpurchased(List<ProductPurchased> productpurchased) {
		this.productpurchased = productpurchased;
	}

	public PaymentHistory getPaymenthistory() {
		return paymenthistory;
	}

	public void setPaymenthistory(PaymentHistory paymenthistory) {
		this.paymenthistory = paymenthistory;
	}

}