package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.model.ServicingEntity;

@Repository
public interface LoginRepo extends JpaRepository<LoginEntity, String> {

	@Query(value = "select * from loginentity where username=?1",nativeQuery = true)
	Optional<LoginEntity> findbyusername(@Param(value = "username") Long username);
	
}
