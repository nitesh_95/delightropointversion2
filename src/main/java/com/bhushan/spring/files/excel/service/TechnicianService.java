package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.http.client.ClientProtocolException;
import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.model.ServicingEntity;
import com.bhushan.spring.files.excel.model.TechnicianPortal;
import com.bhushan.spring.files.excel.repository.ServicingdetailsRepo;
import com.bhushan.spring.files.excel.repository.TechnicianPortals;
import com.bhushan.spring.files.excel.response.ServicingEntityResponsee;
import com.bhushan.spring.files.excel.response.TechnicianPortalResponse;
import com.bhushan.spring.files.excel.response.TechnicianResponse;

@Service
public class TechnicianService<E> {

	@Autowired
	private TechnicianPortals portals;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private ServicingdetailsRepo servicingdetailsRepo;

	public TechnicianPortalResponse saveData(TechnicianPortal portal) throws ClientProtocolException, IOException {
		TechnicianPortalResponse technicianPortalResponse = new TechnicianPortalResponse();
		portal.setProductPurchaseds(portal.getProductPurchaseds());
		double totalAmount = getTotalAmount(portal);
		double paidservicingamount = portal.getPaidservicingamount();
		portal.setRemainingamount(totalAmount - paidservicingamount);
		portal.setTotalcost(totalAmount);
		portal.setAssignedto(portal.getAssignedto());
		Optional<ServicingEntity> findbyticketnoandmobileno = servicingdetailsRepo
				.findbyticketnoandmobileno(portal.getTicketno(), portal.getMobileno());
		findbyticketnoandmobileno.get().setTicketstatus("Completed");
		servicingdetailsRepo.save(findbyticketnoandmobileno.get());
		TechnicianPortal save = portals.save(portal);
		if (save.getTicketstatus().equalsIgnoreCase("Completed")) {
			messageUtil.servicingClientMessage(portal);
			messageUtil.sendmessagetotechnician(portal);
			messageUtil.sendmsgtoadmin(portal);
			technicianPortalResponse.setMessage("Success");
			technicianPortalResponse.setStatus("200");
			technicianPortalResponse.setTechnicianPortal(save);
		} else {
			technicianPortalResponse.setMessage("Failure");
			technicianPortalResponse.setStatus("400");
			technicianPortalResponse.setTechnicianPortal(null);
		}
		return technicianPortalResponse;

	}

	public double getTotalAmount(TechnicianPortal portal) {
		double amt = 0;
		double techniciancost = portal.getTechniciancost();
		ArrayList<Double> arrayList = new ArrayList<Double>();
		for (int i = 0; i < portal.getProductPurchaseds().size(); i++) {
			double amount = portal.getProductPurchaseds().get(i).getUnitprices()
					+ portal.getProductPurchaseds().get(i).getQuantities();
			arrayList.add(amount);
		}
		for (Double double1 : arrayList) {
			amt = amt + double1;
		}
		double amts1 = amt + techniciancost;
		return amts1;
	}

	public TechnicianPortal findbyid(int id) throws Exception {
		Optional<TechnicianPortal> findById = portals.findById(id);
		if (findById.isPresent()) {
			return findById.get();
		} else {
			throw new Exception("Id not Present");
		}
	}

	public List<TechnicianPortal> findall() {
		List<TechnicianPortal> findAll = portals.findAll();
		return findAll;
	}

	public TechnicianResponse technicianPortalCount(String technicianname) {
		TechnicianResponse technicianResponse = new TechnicianResponse();
		List<ServicingEntity> findAll = servicingdetailsRepo.findAll();
		List<ServicingEntity> collect = findAll.stream().filter(i -> technicianname.equals(i.getAssignedto()))
				.collect(Collectors.toList());
		List<TechnicianPortal> collectdata = portals.findAll();
		List<TechnicianPortal> collects = collectdata.stream().filter(i -> "Completed".equals(i.getTicketstatus()))
				.collect(Collectors.toList());
		List<TechnicianPortal> collectsss = collects.stream().filter(i -> technicianname.equals(i.getAssignedto()))
				.collect(Collectors.toList());
		technicianResponse.setAlltasks(collect.size());
		technicianResponse.setCompletedtasks(collectsss.size());
		technicianResponse.setCustomersatisfaction(100);
		technicianResponse.setPendingtasks(collect.size() - collectsss.size());
		return technicianResponse;
	}

}
