package com.bhushan.spring.files.excel.response;

public class CustomerWarrantyResponse {

	private String status;
	private String message;
	private boolean warrantyapplicable;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isWarrantyapplicable() {
		return warrantyapplicable;
	}

	public void setWarrantyapplicable(boolean warrantyapplicable) {
		this.warrantyapplicable = warrantyapplicable;
	}

}
