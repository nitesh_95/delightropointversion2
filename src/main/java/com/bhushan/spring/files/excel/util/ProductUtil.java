package com.bhushan.spring.files.excel.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ProductUtil {

	private static final String SQL_SELECT = "select productname , quantity from productnames;";
	private static final String SQL_SELECTs = "select productnamesdata, quantities from productpurchased;";

	public Map<String, Integer> sensorValuesMaps() throws SQLException {
		String productname = null;
		Integer quantity = 0;
		Map<String, Integer> sensorValues = new HashMap<String, Integer>();
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/delightros", "root", "Nitesh123");
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECTs)) {
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				productname = resultSet.getString("productnamesdata");
				quantity = resultSet.getInt("quantities");
				sensorValues.put(productname, quantity);
			}

		}
		return sensorValues;
	}

	public Map<String, Integer> sensorValuesMap() throws SQLException {
		String productname = null;
		Integer quantity = 0;
		Map<String, Integer> sensorValues = new HashMap<String, Integer>();
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/delightros", "root", "Nitesh123");
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				productname = resultSet.getString("productname");
				quantity = resultSet.getInt("quantity");
				sensorValues.put(productname, quantity);
			}

		}
		return sensorValues;
	}
}
