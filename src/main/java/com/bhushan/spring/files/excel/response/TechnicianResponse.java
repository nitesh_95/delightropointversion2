package com.bhushan.spring.files.excel.response;

public class TechnicianResponse {

	private double completedtasks;
	private double pendingtasks;
	private double alltasks;
	private double customersatisfaction;

	public double getCompletedtasks() {
		return completedtasks;
	}

	public void setCompletedtasks(double completedtasks) {
		this.completedtasks = completedtasks;
	}

	public double getPendingtasks() {
		return pendingtasks;
	}

	public void setPendingtasks(double pendingtasks) {
		this.pendingtasks = pendingtasks;
	}

	public double getAlltasks() {
		return alltasks;
	}

	public void setAlltasks(double alltasks) {
		this.alltasks = alltasks;
	}

	public double getCustomersatisfaction() {
		return customersatisfaction;
	}

	public void setCustomersatisfaction(double customersatisfaction) {
		this.customersatisfaction = customersatisfaction;
	}

}
