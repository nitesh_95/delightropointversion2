package com.bhushan.spring.files.excel.response;

public class CustomerResponse {

	private Response response;

	public Response getResponse() {
		return response;
	}

	public void setResponse(Response response) {
		this.response = response;
	}
	
}
