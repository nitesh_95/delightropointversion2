package com.bhushan.spring.files.excel.response;

public class CustomerWarrantyRequest {

	private int customerid;
	private String name;
	private int productid;
	private String productname;
	private int warrantyDays;
	private String deliverydate;

	public String getDeliverydate() {
		return deliverydate;
	}

	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}

	public int getCustomerid() {
		return customerid;
	}

	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public int getWarrantyDays() {
		return warrantyDays;
	}

	public void setWarrantyDays(int warrantyDays) {
		this.warrantyDays = warrantyDays;
	}

}
