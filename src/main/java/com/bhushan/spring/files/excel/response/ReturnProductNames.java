package com.bhushan.spring.files.excel.response;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bhushan.spring.files.excel.model.Productnames;


@Entity
@Table(name = "returnproductnamesdataas")
public class ReturnProductNames {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Integer purchaseid;
	private double quantities;
	private double unitprices;
	private String productnamesdata;
	private double count;
	private String dateofpurchase;
	private String typeofcustomer;
	@ManyToOne(cascade = { CascadeType.ALL })
	private Productnames productnames;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPurchaseid() {
		return purchaseid;
	}
	public void setPurchaseid(Integer purchaseid) {
		this.purchaseid = purchaseid;
	}
	public double getQuantities() {
		return quantities;
	}
	public void setQuantities(double quantities) {
		this.quantities = quantities;
	}
	public double getUnitprices() {
		return unitprices;
	}
	public void setUnitprices(double unitprices) {
		this.unitprices = unitprices;
	}
	public String getProductnamesdata() {
		return productnamesdata;
	}
	public void setProductnamesdata(String productnamesdata) {
		this.productnamesdata = productnamesdata;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public String getDateofpurchase() {
		return dateofpurchase;
	}
	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}
	public String getTypeofcustomer() {
		return typeofcustomer;
	}
	public void setTypeofcustomer(String typeofcustomer) {
		this.typeofcustomer = typeofcustomer;
	}
	public Productnames getProductnames() {
		return productnames;
	}
	public void setProductnames(Productnames productnames) {
		this.productnames = productnames;
	}
	

}
