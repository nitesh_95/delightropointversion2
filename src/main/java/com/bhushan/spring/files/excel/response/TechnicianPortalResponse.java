package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.TechnicianPortal;

public class TechnicianPortalResponse {

	private String status;
	private String message;
	private TechnicianPortal technicianPortal;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TechnicianPortal getTechnicianPortal() {
		return technicianPortal;
	}

	public void setTechnicianPortal(TechnicianPortal technicianPortal) {
		this.technicianPortal = technicianPortal;
	}

}
