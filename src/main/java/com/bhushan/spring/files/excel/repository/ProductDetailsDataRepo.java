package com.bhushan.spring.files.excel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;

public interface ProductDetailsDataRepo extends JpaRepository<ExcelSheetToDBProductDetails, Integer> {

}
