package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stockproductnames")
public class StockProduct {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	private int itemcode;
	private String itemname;
	private long hsncode;
	private String itemimage;
	private String description;
	private String itemlocation;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemimage() {
		return itemimage;
	}

	public void setItemimage(String itemimage) {
		this.itemimage = itemimage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemlocation() {
		return itemlocation;
	}

	public void setItemlocation(String itemlocation) {
		this.itemlocation = itemlocation;
	}

	public int getItemcode() {
		return itemcode;
	}

	public void setItemcode(int itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public long getHsncode() {
		return hsncode;
	}

	public void setHsncode(long hsncode) {
		this.hsncode = hsncode;
	}

}
