package com.bhushan.spring.files.excel.response;

import java.util.List;

import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.model.ServicingEntity;

public class LoginResponse {
	private String status;
	private String message;
	private String name;
	private List<ServicingEntity> servicingEntities;
	private LoginEntity loginEntity;
	
	
	public List<ServicingEntity> getServicingEntities() {
		return servicingEntities;
	}
	public void setServicingEntities(List<ServicingEntity> servicingEntities) {
		this.servicingEntities = servicingEntities;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public LoginEntity getLoginEntity() {
		return loginEntity;
	}
	public void setLoginEntity(LoginEntity loginEntity) {
		this.loginEntity = loginEntity;
	}
	
	
}
