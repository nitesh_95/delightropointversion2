package com.bhushan.spring.files.excel.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "productpurchased")
public class ProductPurchased {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int purchaseid;
	private int quantities;
	private int unitprices;
	private String productnamesdata;
	private int count;
	private String dateofpurchase;
	private String typeofcustomer;
	@ManyToOne(cascade = { CascadeType.ALL })
	private Productnames productnames;

	public int getPurchaseid() {
		return purchaseid;
	}
	
	public String getDateofpurchase() {
		return dateofpurchase;
	}


	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}


	public String getTypeofcustomer() {
		return typeofcustomer;
	}


	public void setTypeofcustomer(String typeofcustomer) {
		this.typeofcustomer = typeofcustomer;
	}


	public String getProductnamesdata() {
		return productnamesdata;
	}

	public void setProductnamesdata(String productnamesdata) {
		this.productnamesdata = productnamesdata;
	}

	public int getCount() {
		return count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public void setPurchaseid(int purchaseid) {
		this.purchaseid = purchaseid;
	}

	public int getQuantities() {
		return quantities;
	}

	public void setQuantities(int quantities) {
		this.quantities = quantities;
	}

	public int getUnitprices() {
		return unitprices;
	}

	public void setUnitprices(int unitprices) {
		this.unitprices = unitprices;
	}

	public Productnames getProductnames() {
		return productnames;
	}

	public void setProductnames(Productnames productnames) {
		this.productnames = productnames;
	}

}