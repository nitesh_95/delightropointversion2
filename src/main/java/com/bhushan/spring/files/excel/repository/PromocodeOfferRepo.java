package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.PromocodeOffers;

@Repository
public interface PromocodeOfferRepo extends JpaRepository<PromocodeOffers, Integer>{
	
	@Query(value = "select * from codeoffer where promocode=?1",nativeQuery = true)
	Optional<PromocodeOffers> findbyPromoCode(@Param(value = "promocode") String promocode);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE codeoffer SET promocodeused = true  WHERE promocode = ?1",nativeQuery = true)
	int setPromocodeuser(@Param(value = "promocode") String promocode);

}
