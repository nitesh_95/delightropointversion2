package com.bhushan.spring.files.excel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.FeedbackForm;

@Repository
public interface FeedbackRepo extends JpaRepository<FeedbackForm, Integer>{

	@Query(value = "select * from feedbackform where customerid=?1",nativeQuery = true)
	FeedbackForm findbycustomerid(@Param(value = "customerid") int customerid);

}
