package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.ServicingEntity;

public class ServicingEntityResponsee {

	private String status;
	private String message;
	private ServicingEntity servicingEntity;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ServicingEntity getServicingEntity() {
		return servicingEntity;
	}

	public void setServicingEntity(ServicingEntity servicingEntity) {
		this.servicingEntity = servicingEntity;
	}

}
