package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;

@Repository
public interface OnlineCustomerRequestRepo extends JpaRepository<OnlineCustomerRequest, Integer> {

	@Query(value = "select * from productpurchasedata where customerid=?1 and mobileno=?2", nativeQuery = true)
	OnlineCustomerRequest findbycustomeridandmobileno(@Param(value = "customerid") int customerid,
			@Param(value = "mobileno") long mobileno);

	@Query(value = "select * from productpurchasedata where invoiceno=?1 and mobileno=?2", nativeQuery = true)
	Optional<OnlineCustomerRequest> findbyinvoiceandmobileno(@Param(value = "invoiceno") int invoiceno,
			@Param(value = "mobileno") long mobileno);

}
