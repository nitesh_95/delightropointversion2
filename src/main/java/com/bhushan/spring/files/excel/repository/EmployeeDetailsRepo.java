package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.EmployeeEntity;

@Repository
public interface EmployeeDetailsRepo extends JpaRepository<EmployeeEntity, Integer> {
	
	@Query(value = "select * from employeedetails where name=?1",nativeQuery = true)
	Optional<EmployeeEntity> findbyname(@Param(value = "name") String name);

}
